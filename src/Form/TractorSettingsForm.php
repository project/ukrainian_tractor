<?php

namespace Drupal\ukrainian_tractor\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tractor settings Form.
 */
class TractorSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a settings controller.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ukrainian_tractor_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ukrainian_tractor.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('ukrainian_tractor.settings');
    $form['form_settings'] = [
      '#type' => 'radios',
      '#title' => $this->t('Tractor settings'),
      '#default_value' => $settings->get('form_settings'),
      '#options' => [
        'stop' => $this->t('Stop'),
        'typing' => $this->t('Tractor starts by typing'),
        'no_activity' => $this->t('Tractor starts after some time of no activity'),
      ],
    ];
    $form['typing_string'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Typing string'),
      '#description' => $this->t('The tractor will start when this text is typed in'),
      '#default_value' => $settings->get('typing_string') ?? 'fuckputin',
      '#states' => [
        'visible' => [
          ':input[name="form_settings"]' => ['value' => 'typing'],
        ],
      ],
    ];
    $form['delay_no_activity'] = [
      '#type' => 'number',
      '#title' => $this->t('Delay in seconds'),
      '#description' => $this->t('The tractor will start when this defined time passed with no user activity (no mouse movement, nor typing)'),
      '#default_value' => $settings->get('delay_no_activity') ?? 1,
      '#min' => 1,
      '#max' => 3600,
      '#step' => 1,
      '#states' => [
        'visible' => [
          ':input[name="form_settings"]' => ['value' => 'no_activity'],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('ukrainian_tractor.settings');
    $settings->set('form_settings', $form_state->getValue('form_settings'));
    $settings->set('typing_string', $form_state->getValue('typing_string'));
    $settings->set('delay_no_activity', $form_state->getValue('delay_no_activity'));
    $settings->save();
    parent::submitForm($form, $form_state);
  }

}

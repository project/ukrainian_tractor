/**
 * @file
 * Tractor JS functions.
 */

(function($, Drupal) {
  let initialized;
  Drupal.behaviors.ukrainian_tractor = {
    attach(context, settings) {
      this.init();
    },
    init() {
      if (initialized) {
        return;
      }
      initialized = true;

      console.log('Drupal Settings: ', drupalSettings.ukrainian_tractor);

      function findAndMarkCharacters(character) {
        $('p').find(':contains(' + character + ')').each(function() {
            $(this).html(function (_, html) {
              return html.replace(new RegExp(character, 'g'), '<span class="found">' + character + '</span>');
            });
          });
      }

      function groupTargetsByLine(targets) {
        const offsetCollection = {};

        targets.each(function() {
          const offsetTop = $(this).offset().top;
          if (offsetCollection[offsetTop]) {
            offsetCollection[offsetTop].push($(this));
          } else {
            offsetCollection[offsetTop] = [$(this)];
          }
        });

        return offsetCollection;
      }

      function moveTractorToNextHeight(tractor, target) { // should take element
        const containerTop = $('body').offset().top;
        const targetPosition = target.offset().top - containerTop - tractor.height() + target.height(); // (this should be the element height);

        tractor.animate({ top: targetPosition, left: 0 - tractor.width() }, 0);
      }

      function animateTractorToElement(tractor, target) {
        // Create a new div for the flag
        const flagContainer = $('<div>').addClass('flag-container');
        const flag = $('<span>').addClass('flag').html('🇺🇦');

        // Append the flag to the tractor
        tractor.prepend(flagContainer);
        flagContainer.html(flag);

        const targetLeft = target.offset().left + target.width();
        const targetTop = target.offset().top - tractor.height() + target.height();

        tractor.animate({ top: targetTop, left: targetLeft }, 2000, function() {
          putFlagInPlace(tractor, target, flagContainer, flag);
        });
      }

      function putFlagInPlace(tractor, element, flagContainer, flag) {
        setTimeout(function () {
          const characterToReplace = 'z';
          element.html(element.html().replace(new RegExp(characterToReplace, 'g'), ''));
          element.append(flag); // Append the flag to the target element
          element.removeClass('found');
          element.addClass('replaced');
          flagContainer.remove();
        }, 10);
      }

      function moveTractorOutOfSight(tractor) {
        tractor.animate({left: window.innerWidth}, 1000);
      }

      $(document).ready(() => {

        const tractor = $(document.createElement('div')); // Create tractor using jQuery
        tractor.addClass('tractor');
        tractor.css({
          display: 'block',
          position: 'absolute',
          top: 0,
        });
        const tractorImage = $(document.createElement('div'));
        tractorImage.addClass('tractor-image');
        tractorImage.css({
          width: '50px',
          height: '50px',
          backgroundColor: 'red',
          zIndex: 99999,
          backgroundImage: 'url("/modules/custom/ukrainian_tractor/img/tractor.png")',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center',
          backgroundSize: 'contain',
        });

        $('body').append(tractor.append(tractorImage)); // Append the tractor to the body using jQuery

        console.log('tractor', tractor);

        const targetCharacter = 'z';
        findAndMarkCharacters(targetCharacter);
        const foundTargets = $('.found');
        console.log('found', foundTargets);

        if (foundTargets.length > 0) {
          const groupedTargets = groupTargetsByLine(foundTargets);
          console.log(groupedTargets);

          Object.keys(groupedTargets).forEach(function(offsetTop) {
            console.log(offsetTop);
            const group = groupedTargets[offsetTop];
            moveTractorToNextHeight(tractor, group[0]);
            for (const element of group) {
              animateTractorToElement(tractor, element);
            }
            moveTractorOutOfSight(tractor);
          });
        }

      });

    },
  };
})(jQuery, Drupal);
